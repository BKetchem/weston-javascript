			function nameGenerate() {
			   var date = new Date(), dateArray = new Array(), i;
			   curYear = date.getFullYear();
				for(i = 0; i<5; i++) {
					dateArray[i] = curYear+i;
				}
				return dateArray;
			}

			function addSelect(divname) {
			   var newDiv=document.createElement('div');
			   var html = '<select>', names = nameGenerate(), i;
			   for(i = 0; i < names.length; i++) {
				   html += "<option value='"+names[i]+"'>"+names[i]+"</option>";
			   }
			   html += '</select>';
			   newDiv.innerHTML= html;
			   document.getElementById(divname).appendChild(newDiv);
			}