All code for weston has been placed here. This is the result of a lot of testing to make Javascript modifications.

The Javascripts folder contains all of the modified Javascripts for the Weston website.

The Weston web service and rest service are c# applications designed to be passed a user and return a list of all users in that department.
--Sample data for the services would be 'aabdou'
the web service is SOAP
the rest service is REST

The xml folder contains all of the xml modifications that were made to the Weston Website.