﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Net;
using System.IO;

namespace WestonRestService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Weston" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Weston.svc or Weston.svc.cs at the Solution Explorer and start debugging.
    public class Weston : IWeston
    {
        #region IWeston Members

        //Test Method
        public string XMLData(string userId)
        {
            string returnList = "";
            SqlConnection weston = new SqlConnection("Data Source = xencitydb01;Initial Catalog=Cityworks_weston_config;User ID=sa;Password=d0wnt0wnXEN");
            SqlDataReader rdr = null;
            weston.Open();
            string sql = "Select firstname + ' '  + lastname from azteca.EMPLOYEE where ORGANIZATION = (Select ORGANIZATION from azteca.EMPLOYEE where LOGINNAME = @userId)";
            SqlCommand cmd = new SqlCommand(sql, weston);
            cmd.Parameters.AddWithValue("@userId", userId);

            rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                returnList = returnList + " " + rdr[0];
            }
            weston.Close();
            return returnList;
        }

        //Test Method
        public string JSONData(string userId)
        {

            string returnList = "";
            SqlConnection weston = new SqlConnection("Data Source = xencitydb01;Initial Catalog=Cityworks_weston_config;User ID=sa;Password=d0wnt0wnXEN");
            SqlDataReader rdr = null;
            weston.Open();
            string sql = "Select firstname + ' '  + lastname from azteca.EMPLOYEE where ORGANIZATION = (Select ORGANIZATION from azteca.EMPLOYEE where LOGINNAME = @userId)";
            SqlCommand cmd = new SqlCommand(sql, weston);
            cmd.Parameters.AddWithValue("@userId", userId);

            rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                returnList = returnList + " " + rdr[0];
            }
            weston.Close();
            return returnList;
        }

        #endregion
    }
}
