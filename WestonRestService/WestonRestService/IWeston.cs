﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.ServiceModel.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Net;
using System.IO;


namespace WestonRestService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWeston" in both code and config file together.
    [ServiceContract]
    public interface IWeston
    {
        [OperationContract]
        [WebInvoke(Method = "Get",
            ResponseFormat = WebMessageFormat.Xml,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "xml/{id}")]
        string XMLData(string id);
        
        [OperationContract]
        [WebInvoke(Method = "Get",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/{id}")]
        string JSONData(string id);
    }
}
