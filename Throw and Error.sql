USE [Cityworks_Weston_Config]
GO
/****** Object:  Trigger [azteca].[CINT_ActualFinishDate]    Script Date: 9/30/2015 1:27:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Brian Ketchem : Timmons Group>
-- Create date: <9/22/2015>
-- Description:	<Prevent the user from inserting an actual finish date 
--				that is greater than the system date>
-- =============================================
ALTER TRIGGER [azteca].[CINT_ActualFinishDate] 
   ON  [azteca].[WORKORDER]
   AFTER insert, update
AS 
BEGIN
	DECLARE 
	@insActFinDat datetime,
	@CurrentDate  datetime,
	@finActFinDat datetime,
	@insWOID nvarchar;
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Select	@insActFinDat = actualfinishdate,
			@currentdate = sysdatetime(),
			@insWOID = (select Workorderid from inserted)
	FROM inserted
	--BEGIN TRY
		IF (@insActFinDat >  @CurrentDate)
		BEGIN;
			RAISERROR('ACTUAL FINISH DATE CANNOT BE GREATER THAN CURRENT DATE', 11, 11)
			UPDATE azteca.WORKORDER
			set ACTUALFINISHDATE = @CurrentDate
			where WORKORDERID = @insWOID
		END;
	--END TRY
	--BEGIN CATCH
		--SELECT ERROR_MESSAGE();
	--END CATCH
END
