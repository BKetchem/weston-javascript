﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Net;
using System.IO;

namespace WestonWebService
{
    /// <summary>
    /// returns list of people also in the department of the userid
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service1 : System.Web.Services.WebService
    {
        [WebMethod]
        public List<string> getDepartmentEmployees(string userId)
        {
            List<string> returnList = new List<string>();
            SqlConnection weston = new SqlConnection("Data Source = xencitydb01;Initial Catalog=Cityworks_weston_config;User ID=sa;Password=d0wnt0wnXEN");
            SqlDataReader rdr = null;
            weston.Open();
            string sql = "Select firstname + ' '  + lastname from azteca.EMPLOYEE where ORGANIZATION = (Select ORGANIZATION from azteca.EMPLOYEE where LOGINNAME = @userId)";
            SqlCommand cmd = new SqlCommand(sql, weston);
            cmd.Parameters.AddWithValue("@userId", userId);

            rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                returnList.Add((rdr[0]).ToString());
            }
            weston.Close();
            return returnList;
        }
        [WebMethod]
        //Test Method
        public string getDepartmentEmployeesString(string userId)
        {
            string returnList = "";
            SqlConnection weston = new SqlConnection("Data Source = xencitydb01;Initial Catalog=Cityworks_weston_config;User ID=sa;Password=d0wnt0wnXEN");
            SqlDataReader rdr = null;
            weston.Open();
            string sql = "Select firstname + ' '  + lastname from azteca.EMPLOYEE where ORGANIZATION = (Select ORGANIZATION from azteca.EMPLOYEE where LOGINNAME = @userId)";
            SqlCommand cmd = new SqlCommand(sql, weston);
            cmd.Parameters.AddWithValue("@userId", userId);

            rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                returnList = returnList + " " + rdr[0];
            }
            weston.Close();
            return returnList;
        }
    }
}