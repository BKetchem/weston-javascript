﻿function cbo_Changed(clientId) {
    dateControl = igedit_getById(clientId);
    if (dateControl) {
        dateControl.setValue(new Date());
    }

    return true;
}

function chkUnitDescLock_Click(chkbox) {
    if (typeof (cboUnitAccompDescId) != 'undefined') {
        var oCbo = document.getElementById(cboUnitAccompDescId);
        if (oCbo) {
            if (chkbox.checked)
                oCbo.disabled = true;
            else
                oCbo.disabled = false;
        }
    }
}

function checkBox_Clicked(chkClientID, CancelledByClientID, DateCancelledClientID, CancelledBy, CancelReasonId) {

    var varCancel, varBy, varDateCancelled, varReason, varStatus;
    varCancel = document.getElementById(chkClientID);
    varBy = document.getElementById(CancelledByClientID);
    var dateControl = igedit_getById(DateCancelledClientID);
    varReason = document.getElementById(CancelReasonId);
    varStatus = document.getElementById(woStatus);

    if (varCancel != null) {
        if (varCancel.checked) {
            if (varBy != null) {
                varBy.value = CancelledBy;
                varBy.disabled = false;
                varBy.readOnly = true;
            }
            if (dateControl) {
                dateControl.setValue(new Date());
                dateControl.setReadOnly(false);
                $('#ctl00_Main_calDateCancelled_imgDrop').show();
            }
            if (varReason != null) {
                varReason.disabled = false;
            }
            if (varStatus != null) {
                varStatus.disabled = true;
                setInputValue(varStatus, 'CANCEL');
            }
        } else {
            if (varBy != null) {
                varBy.value = "";
                varBy.disabled = true;
                varBy.readOnly = false;
            }
            if (dateControl) {
                dateControl.setValue(null);
                dateControl.setReadOnly(true);
                $('#ctl00_Main_calDateCancelled_imgDrop').hide();
            }
            if (varReason != null) {
                varReason.value = "";
                varReason.disabled = true;
            }
            if (varStatus != null) {
                if (varStatus.value == 'CANCEL')
                    setInputValue(varStatus, '');
                varStatus.disabled = false;
            }
        }
    }

    return true;
}

function setState_calFromDate(isDisabled, calId) {
    var oCal = document.getElementById(calId + '_tblWrap');
    var oImg = document.getElementById(calId + '_imgDrop');

    if (isDisabled == true) {
        if (oCal)
            oCal.style.display = 'none';
        if (oImg)
            oImg.style.display = 'none';
    } else {
        if (oCal)
            oCal.style.display = '';
        if (oImg)
            oImg.style.display = '';
    }
}

function cboCycleFrom_Changed(cbo, calId) {
    var oVal = getInputValue(cbo, ',');
    var isDisabled = oVal != 'ADATE';
    setState_calFromDate(isDisabled, calId);
    return true;
}

function validateFormFields(boolValidateRequiredFields, boolCheckSuppressCycle) {
    var msg = '';
    var msgId = '';
    var res = true;
    cw.Utilities.showHideElement([successId, warningId, errorId], 'none');

    //check requiredFields
    if (boolValidateRequiredFields == true) {
        if (!cw.Pages.validateRequiredFields()) {
            msg = cw.LayoutManagers.WOGeneral.Messages.CompleteRequiredFields;
            msgId = errorId;
        }
    }
    //check projected finish date
    if (msg == '') {
        if (!cw.Utilities.validateFinishDate(projStartInputId, projFinishInputId)) {
            msg = cw.LayoutManagers.WOGeneral.Messages.InvalidProjectedFinishDate;
            msgId = warningId;
        }
    }

    var actualStartInputId = "ctl00_Main_calActualStartDate_datInpt";
    var actualFinishInputId = "ctl00_Main_calActualFinishDate_datInpt";

    //check actual finish date
    if (msg == '') {
        if (!cw.Utilities.validateFinishDate(actualStartInputId, actualFinishInputId)) {
            msg = cw.LayoutManagers.WOGeneral.Messages.InvalidActualFinishDate;
            msgId = warningId;
        }
    }
    //display error if needed.
    if (msg != '') {
        cw.Pages.showBanner(msg, msgId);
        res = false;
    }
    if (boolCheckSuppressCycle && !cw.Utilities.boolReopening && $get(hidSupressCycle).value == '')
        res = false;
    return res;
}

function verifyDelete() {
    var oId = document.getElementById(cboCwId);
    var oIdValue = '';
    if (oId)
        oIdValue = getInputValue(oId, ',');

    var deleteTitle = cw.LayoutManagers.WOGeneral.Messages.DeleteWOTitle + ' ' + oIdValue;
    var deleteMessage = cw.LayoutManagers.WOGeneral.Messages.DeleteWOText;
    return cw.Utilities.confirmCwItem(btnDeleteId, deleteTitle, deleteMessage);
}

function parseAutoComplete_WO(itm, args) {
    //itm, args, delimiter, cityId, stateId, zipId
    cw.Utilities.parseStreetCodes(itm, args, '\t', '', '', '');
}

// Create the project name tree.
var projectNameTree;
Ext.onReady(function () {
    var projectNameTreeLoader = new Ext.tree.TreeLoader({ dataUrl: 'WOGeneralEdit.aspx?ajaxcall=projecttree' });
    projectNameTreeLoader.on('beforeload', function (treeLoader, node) {
        treeLoader.baseParams.id = node.attributes.id;
    });
    projectNameTree = new Ext.tree.TreePanel({
        loader: projectNameTreeLoader,
        animate: false,
        autoScroll: true,
        root: new Ext.tree.AsyncTreeNode({ text: cw.LayoutManagers.WOGeneral.Messages.ProjectTreeRoot, id: '0' })
    });
    projectNameTree.on('click', function (node) {
        $('#ctl00_Main_cboProjectName').val(node.id);
        projectNameTree.collapseAll();
    });
    projectNameTree.render('projectNameTree');

    var saveBtn = document.getElementById(cmdSaveId);
	if(saveBtn)
		Ext.EventManager.on(cmdSaveId, 'click', confirmCreateCycle(cmdSaveId));
});

var changeEntityTypeWindow;

function hideEntityType() {
    changeEntityTypeWindow.hide();
    cw.Utilities.showHideElement(commentsDisableId, 'none');
}

function showEntityType() {
    var btn = $get(btnShowEntityTypeId);
    btn.click();
}

function showEntityTypeWindow() {
    var div = Ext.getDom('divChangeEntityType');
    if (!changeEntityTypeWindow) {
        changeEntityTypeWindow = new Ext.Window({
            renderTo: div.parentNode,
            layout: 'fit',
            width: 500,
            height: 296,
            closeAction: 'hide',
            plain: true,
            closable: false,
            resizable: false,
            title: 'Select Template',
            x: 50,
            y: 150,
            items: new Ext.Panel({
                id: 'tbChangeEntity',
                border: false,
                items: [{ id: 'tbChangeEntity', contentEl: div }]
            }),
            buttons: [{
                id: 'btnChangeEntity',
                text: messages.Select,
                disabled: false,
                handler: function () {
                    __doPostBack(Ext.getDom(btnApplyEntityTypeId).name, '');
                }
            }, {
                id: 'btnCloseChangeEntity',
                text: messages.Close,
                handler: hideEntityType
            }]
        });
    }
    cw.Utilities.showHideElement(commentsDisableId, 'block');
    changeEntityTypeWindow.show();
}

function confirmCreateCycle(btnId) {
    var btn = btnId;
    return function (e) {
        var cycleFrom = $get(cboCycleFromId).value, cycleType = $get(cboCycleTypeId).value.toUpperCase();
        var suppressCycle = $get(hidSupressCycle);
        var hasCycleWOs = $get(hidHasCycleWOsId).value == 'true';
        var msgs = cw.LayoutManagers.WOGeneral.Messages;
        var noCycleDueToprojectState = $get(cboStageId).value.toUpperCase() == 'PROPOSED';
        if (!noCycleDueToprojectState &&
            cycleFrom == 'PROJSTARTDATE' &&
            cycleType != 'NEVER' &&
            !hasCycleWOs &&
            !yesNoPrompt(msgs.CreateCycleWOPromptTitle, msgs.CreateCycleWOPrompt, function (result) {
                    if (result == 'cancel')
                        return;
                    suppressCycle.value = result == 'no';
                    cw.Window.performButtonClick(btn);
        }
            )
        ) {
            e.preventDefault();
        } else {
            suppressCycle.value = false;
            cw.Window.performButtonClick(btn);
        }
    };
}

var _bypassYesNoMessage = false;

function yesNoPrompt(title, text, callback) {
    if (_bypassYesNoMessage) {
        _bypassYesNoMessage = false;
        return true;
    }

    Ext.Msg.show({
        title: title,
        msg: text,
        buttons: Ext.Msg.YESNOCANCEL,
        fn: function (result) {
            _bypassYesNoMessage = result != 'cancel';
            callback(result);
        },
        icon: Ext.MessageBox.QUESTION
    });
    return false;
}

require(['js/shortcuts'], function (Shortcuts) {
    var shortcuts = new Shortcuts();
    shortcuts.init(shortcuts.pages.WOGenEdit);
});