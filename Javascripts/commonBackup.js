﻿// Additional string functions.
String.prototype.trim = function () {
	return this.replace(/^\s+|\s+$/g, "");
};
String.prototype.ltrim = function () {
	return this.replace(/^\s+/, "");
};
String.prototype.rtrim = function () {
	return this.replace(/\s+$/, "");
};

//Adds a Javascript function to the specified window's window.onload event. Guarantees that any function sent through this function will be called after the page loads and all nested scripts are also ran.
function addLoadEvent(oFunction, oWindow) {
	if ((!oWindow) || (typeof oWindow != "object"))
		oWindow = window;
	var oOldOnload = oWindow.onload;
	if (typeof oWindow.onload != "function")
		oWindow.onload = oFunction;
	else {
		oWindow.onload = function () { oOldOnload(); oFunction(); };
	}
}

function clearLoadEvents(oWindow) {
	if ((!oWindow) || (typeof oWindow != "object"))
		oWindow = window;
	oWindow.onload = null;
}

// Adds a function to a special variable which is called after the page "initializes".
var postLoadEvents = function () { };
function addPostLoadEvent(oFunction) {
	if (typeof oFunction == "function") {
		var oOldPostLoadEvents = postLoadEvents;
		postLoadEvents = function () { oOldPostLoadEvents(); oFunction(); };
	}
}

function clearPostLoadEvents() {
	postLoadEvents = function () { };
}

//Adds a Javascript function to the specified window's window.onunload event. Guarantees that any function sent through this function will be called before the page unloads.
function addUnloadEvent(oFunction, oWindow) {
	if ((!oWindow) || (typeof oWindow != "object"))
		oWindow = window;
	var oOldOnload = oWindow.onunload;
	if (typeof oWindow.onunload != "function")
		oWindow.onunload = oFunction;
	else {
		oWindow.onunload = function () { oOldOnload(); oFunction(); };
	}
}

function clearUnloadEvents(oWindow) {
	if ((!oWindow) || (typeof oWindow != "object"))
		oWindow = window;
	oWindow.onunload = null;
}

// Returns true if the object is an array, else false
function isArray(obj) { if (obj != null) { if (obj.length) return (typeof (obj.length) == "undefined") ? false : true; } return false; }

// Takes an array of values and turns it into a comma-separated string. A delimeter can also be specified.
function commifyArray(obj, delimiter) {
	if (typeof (delimiter) == "undefined" || delimiter == null) {
		delimiter = ",";
	}
	var s = "";
	if (obj == null || obj.length <= 0) { return s; }
	for (var i = 0; i < obj.length; i++) {
		s = s + ((s == "") ? "" : delimiter) + obj[i].toString();
	}
	return s;
}

// Utility function used by others.
function getSingleInputText(obj, use_default, delimiter) {
	if (obj != null) {
		if (obj.type) {
			switch (obj.type) {
				case "radio": return "";
				case "checkbox":
					return (use_default) ? obj.defaultChecked : obj.checked;
				case "text": case "hidden": case "textarea": return (use_default) ? obj.defaultValue : obj.value;
				case "password": return ((use_default) ? null : obj.value);
				case "select-one":
					if (obj.options == null) { return null; }
					if (use_default) {
						var o = obj.options;
						for (var i = 0; i < o.length; i++) { if (o[i].defaultSelected) { return o[i].text; } }
						return o[0].text;
					}
					if (obj.selectedIndex < 0) { return null; }
					return (obj.options.length > 0) ? obj.options[obj.selectedIndex].text : null;
				case "select-multiple":
					if (obj.options == null) { return null; }
					var values = new Array();
					for (var i = 0; i < obj.options.length; i++) {
						if ((use_default && obj.options[i].defaultSelected) || (!use_default && obj.options[i].selected)) {
							values[values.length] = obj.options[i].text;
						}
					}
					return (values.length == 0) ? null : commifyArray(values, delimiter);
			}
		}
		else if (obj.tagName) {
			switch (obj.tagName.toString().toLowerCase()) {
				case "label":
				case "span":
				case "div":
					return obj.innerHTML;
			}
		}
	}
	if (typeof console !== "undefined")
		console.log("Field type " + obj.type + " cannot be used with the getInput functions. (superCombo.js:getSingleInputText:e1)");
	return null;
}

// Gets the displayed text of any form input field. Multiple-select fields are returned as delmited values. Doesn't work with buttons, file pickers, reset buttons, and submit buttons.
function getInputText(obj, delimiter) {
	var use_default = (arguments.length > 2) ? arguments[2] : false;
	if (isArray(obj) && (typeof (obj.type) == "undefined")) {
		var values = new Array();
		for (var i = 0; i < obj.length; i++) {
			var v = getSingleInputText(obj[i], use_default, delimiter);
			if (v != null) { values[values.length] = v; }
		}
		return commifyArray(values, delimiter);
	}
	return getSingleInputText(obj, use_default, delimiter);
}

// Utility function used by others.
function getSingleInputValue(obj, use_default, delimiter) {
	if (!obj) return '';
	if (obj.type) {
		switch (obj.type) {
			case "radio": case "checkbox":
				return (((use_default) ? obj.defaultChecked : obj.checked) ? obj.value : null);
			case "text": case "hidden": case "textarea":
				return (use_default) ? obj.defaultValue : obj.value;
			case "password":
				return ((use_default) ? null : obj.value);
			case "select-one":
				if (obj.options == null)
					return null;
				if (use_default) {
					var o = obj.options;
					for (var i = 0; i < o.length; i++) {
						if (o[i].defaultSelected)
							return o[i].value;
					}
					return o[0].value;
				}
				if (obj.selectedIndex < 0)
					return null;
				return (obj.options.length > 0) ? obj.options[obj.selectedIndex].value : null;
			case 'select-multiple':
				if (obj.options == null)
					return null;
				var values = new Array();
				for (var i = 0; i < obj.options.length; i++) {
					if ((use_default && obj.options[i].defaultSelected) || (!use_default && obj.options[i].selected)) {
						values[values.length] = obj.options[i].value;
					}
				}
				return (values.length == 0) ? null : commifyArray(values, delimiter);
		}
	}
	else if (obj.tagName) {
		switch (obj.tagName.toString().toLowerCase()) {
			case "label":
			case "span":
			case "div":
				return obj.innerHTML;
		}
	}
	if (typeof console !== "undefined")
		console.log("ERROR: Field type " + obj.type + " is not supported for this function");
	return null;
}

// Gets the value of any form input field. Multiple-select fields are returned as delimited values. Doesn't work with buttons, file pickers, reset buttons, and submit buttons.
function getInputValue(obj, delimiter) {
	var use_default = (arguments.length > 2) ? arguments[2] : false;
	if (isArray(obj) && (typeof (obj.type) == "undefined")) {
		var values = new Array();
		for (var i = 0; i < obj.length; i++) {
			var v = getSingleInputValue(obj[i], use_default, delimiter);
			if (v != null) {
				values[values.length] = v;
			}
		}
		return commifyArray(values, delimiter);
	}
	return getSingleInputValue(obj, use_default, delimiter);
}

// Utility function used by others.
function setSingleInputValue(obj, value) {
	if (obj.type) {
		switch (obj.type) {
			case 'radio': case 'checkbox':
				if (value == true || value == 'checked') {
					obj.checked = true;
					return true;
				}
				else {
					obj.checked = false;
					return false;
				}
			case 'text': case 'hidden': case 'textarea': case 'password':
				obj.value = value;
				return true;
			case 'select-one': case 'select-multiple':
				var o = obj.options;
				for (var i = 0; i < o.length; i++) {
					//(JO) check both value and displayed text for match
					if (o[i].value == value) {
						o[i].selected = true;
					}
					else if ((o[i].innerHTML) && (o[i].innerHTML == value)) {
						o[i].selected = true;
					}
					else if ((o[i].innerText) && (o[i].innerText == value)) {
						o[i].selected = true;
					}
					else {
						o[i].selected = false;
					}
				}
				return true;
		}
	}
	else if (obj.tagName) {
		switch (obj.tagName.toString().toLowerCase()) {
			case "label":
			case "span":
			case "div":
				obj.innerHTML = value;
				return true;
		}
	}
	if (typeof console !== "undefined")
		console.log("ERROR: Field type " + obj.type + " is not supported in the setSingleInputValue function.");
	return false;
}

// Set the value of any form field. In cases where no matching value is available (select, radio, etc) then no option will be selected.
function setInputValue(obj, value) {
	var use_default = (arguments.length > 1) ? arguments[1] : false;
	if (isArray(obj) && (typeof (obj.type) == "undefined")) {
		for (var i = 0; i < obj.length; i++) {
			setSingleInputValue(obj[i], value);
		}
	}
	else {
		setSingleInputValue(obj, value);
	}
}

// Returns the closest ancestor to the specified node with the specified id. 
function getAncestorNode(currentNode, strID) {
	if (currentNode == null)
		return null;
	if ((currentNode.id != null) && (currentNode.id.substr(currentNode.id.length - 7) == strID))
		return currentNode;
	if (currentNode.parentNode == null)
		return null;
	return getAncestorNode(currentNode.parentNode, strID)
}

// Returns the closest decendant to the specified node with the specified id.
function getDescendantNode(currentNode, strID) {
	if (!currentNode)
		return null;

	var childArray;
	if (currentNode.id != null) {
		childArray = currentNode.id.substr(currentNode.id.length - 7);
		if (childArray == strID)
			return currentNode;
	}
	childArray = currentNode.childNodes;
	for (var i = 0; i < childArray.length; i++) {
		var oNode = getDescendantNode(childArray[i], strID);
		if (oNode != null)
			return oNode;
	}
	return null;
}

// Determines if the passed event contains a key stroke which was a number, backspace, or delete.
function validNum(evt) {
	evt = (evt) ? evt : ((window.event) ? window.event : null);
	if (evt) {
		var keyCode = (evt.keyCode) ? evt.keyCode : evt.which;
		if (((keyCode > 47) && (keyCode < 58)) || (keyCode == 8)) {
			if (typeof evt != "undefined")
				return evt.returnValue = true;
			else
				return false;
		}
	}
	if (typeof evt != "undefined")
		return evt.returnValue = false;
	else
		return false;
}

// Function which checks and validates required fields passed through arrRequiredFields from the server.
function validateRequiredFields() {
	return cw.Pages.validateRequiredFields();
}

// Toggles the expand state of all groups within a grid.
var bolGridExpanded = true;
function toggleExpandState(gridId) {
	var oGrid = $find(gridId);
	if (oGrid) {
		if (oGrid.bolGridSummarized)
			oGrid.bolGridSummarized = false;
		oGrid.bolGridExpanded = oGrid.bolGridExpanded == undefined ? false : !oGrid.bolGridExpanded;
		var rows = oGrid.MasterTableView.get_element().rows;
		for (var i = 0; i < rows.length; i++) {
			var button = oGrid.MasterTableView._getGroupExpandButton(rows[i]);
			if (button && button.id.split("__")[2] == "0") {
				if (button.className.indexOf("rgExpand") > -1 && oGrid.bolGridExpanded || !oGrid.bolGridExpanded && button.className.indexOf("rgCollapse") > -1)
					oGrid.MasterTableView._toggleGroupsExpand(button, { "groupLevel": button.id.split("__")[2] });
			}
		}
	}
	else if (igtbl_getGridById) {// Only works with one grid per page. This should be removed later.
		var oGrid = igtbl_getGridById(gridId);
		if (oGrid) {
			bolGridExpanded = !bolGridExpanded;
			for (var i = 0, length = oGrid.Rows.length; i < length; i++)
				oGrid.Rows.getRow(i).setExpanded(bolGridExpanded);
		}
	}
}

// Turn a date and time object into a string
// EX: 02/12/2008 14:55
function getDateTimeString(date) {
	// Create a function to add a leading zero if necessary
	zerof = function (value) { if (value < 10) return "0" + value; else return value.toString(); }

	// Return the formatted string.
	return zerof(date.getMonth() + 1) + "/" + zerof(date.getDate()) + "/" + zerof(date.getFullYear()) + " " + zerof(date.getHours()) + ":" + zerof(date.getMinutes())
}


if ((window.name != "CwMap") && (document.title) && (document.title != "")) {
	top.document.title = document.title;
}

/************************* Namespaced functions and variables start here! *************************/

/***** cw namespace *****/
if (typeof (cw) == 'undefined') {
	var cw = {};
}

/***** cw.Pages namespace *****/
cw.Pages = function () {
	var fieldLabelMap = [];

	// ids for banners
	var successBannerId;
	var warningBannerId;
	var errorBannerId;

	return {
		registerRequiredField: function (fieldId, linkedControlId) {
			fieldLabelMap[fieldId] = linkedControlId;
		},
		getRequiredFields: function () {
			return fieldLabelMap;
		},
		validateRequiredFields: function () {
			var ret = true;
			var fieldLabelMap = cw.Pages.getRequiredFields();
			for (fieldId in fieldLabelMap) {
				var field = document.getElementById(fieldId);

				// See if the field is valid.
				if (field) {
					var valid = cw.Pages.validateField(field);

					//set the control or linked control if it exists.
					var label = $("#" + fieldLabelMap[fieldId]);
					if (!label)
						label = field.parentNode;

					if (valid) {
						$(label).removeClass('required');
					} else {
						$(label).addClass('required');
						ret = false;
					}
				}
			}
			return ret;
		},
		validateForm: function (bannerId) {
			var valid = cw.Pages.validateRequiredFields();
			// if not valid show an error message
			if (!valid) {
				// get the error message
				var msg = cw.Utilities.getFirstLayoutManager().Messages.RequiredFieldsError;
				// show the error message banner
				cw.Pages.showBanner(msg, bannerId, true);
				// return an error
				return false;
			};
			return true;
		},
		validateTextBoxNumericRangeSelectorInputs: function (bannerId) {

            //There can't be any range selectors without this being defined
		    if (!textBoxRangeSelector)
		        return true;

		    var itemsWithErrors = textBoxRangeSelector.validateAllTextBoxRangeSelectorsOnPage();

		    if (!itemsWithErrors || itemsWithErrors.length == 0)
		        return true;

		    var errorMsg = "The numeric ranges in the following fields are not properly formatted: ";
		    for (var cnt = itemsWithErrors.length, i = 0; i < cnt; i++) {

		        errorMsg += itemsWithErrors[i];
		        if (i !== (cnt - 1))
		            errorMsg += ", ";
		    }

		    cw.Pages.showErrorBanner(errorMsg);
		    return false;
		},
		// Built in simple validation good for any field.
		validateField: function (oNode) {
			var valid = true;
			var tag = oNode.tagName.toLowerCase();
			if (tag === "input" || tag === "select" || tag === "textarea") {
				var value = getInputValue(oNode);
				valid = !(value === "" || value === null);
			} else if (tag === "table") {
				var inputs = oNode.getElementsByTagName("input");
				if (inputs.length > 0) {
					valid = false;
					for (var i = 0; i < inputs.length; i++) {
						if (inputs[i].checked) {
							valid = true;
							break;
						}

					}
				}
			}
			return valid;
		},

		showBanner: function (bannerText, bannerId, autohide) {
			// set the default autohide behavior
			autohide = typeof (autohide) == 'undefined' ? true : autohide;

			if (bannerText != '') {
				if (bannerId) {
					var lbl = document.getElementById(bannerId);
					if (lbl) {
						lbl.innerText = bannerText;
						lbl.innerHTML = bannerText;
						cw.Utilities.showHideElement(bannerId, '');

						// autohide banner
						var TIMEOUT = 5000;
						if (autohide)
							setTimeout('cw.Pages.hideBanner("' + bannerId + '");', TIMEOUT);
					}
				}
				else
					alert(bannerText);
				res = false;
			}
		},
		hideBanner: function (bannerId) {
			if (bannerId) {
				var lbl = document.getElementById(bannerId);
				if (lbl) {
					lbl.innerText = '';
					lbl.innerHTML = '';
					cw.Utilities.showHideElement(bannerId, 'none');
				}
			}
		},
		setSuccessBannerId: function (id) {
			successBannerId = id;
		},
		showSuccessBanner: function (text) {
			cw.Pages.showBanner(text, successBannerId);
		},
		setWarningBannerId: function (id) {
			warningBannerId = id;
		},
		showWarningBanner: function (text) {
			cw.Pages.showBanner(text, warningBannerId);
		},
		setErrorBannerId: function (id) {
			errorBannerId = id;
		},
		showErrorBanner: function (text) {
			cw.Pages.showBanner(text, errorBannerId);
		}
	}; // End of cw.Pages namespace.
}();

/***** cw.Window namespace *****/
cw.Window = function () {
	// Private members.
	var strElementIdToFocus;
	var bolHandleFormKeystrokes = false;
	var controlIdsToSkip = '';
	return {
		// Sets the initial page focus to the passed field.
		setInitialFocus: function (strTargetId) {
			strElementIdToFocus = strTargetId;
			addLoadEvent(cw.Window.focusField);
		},

		// Resize the window to the passed width and height.
		setSize: function (width, height) {
			if (window.outerWidth) {
				window.outerWidth = width;
				window.outerHeight = height;
			}
			else if (window.resizeTo)
				window.resizeTo(width, height);
		},

		// Get the window's height in pixels. null is returned if getting the window height failed.
		getHeight: function () {
			var intHeight = 600;
			if (typeof (window.innerHeight) == 'number')
				intHeight = window.innerHeight; // Non-IE
			else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight))
				intHeight = document.documentElement.clientHeight; // IE 6+ in 'standards compliant mode'
			else if (document.body && (document.body.clientWidth || document.body.clientHeight))
				intHeight = document.body.clientHeight; // IE 4 compatible
			return intHeight;
		},

		// Get the window's height in pixels. null is returned if getting the window height failed.
		getWidth: function () {
			var intWidth;
			if (typeof (window.innerWidth) == 'number')
				intWidth = window.innerWidth; // Non-IE
			else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight))
				intWidth = document.documentElement.clientWidth; // IE 6+ in 'standards compliant mode'
			else if (document.body && (document.body.clientWidth || document.body.clientHeight))
				intWidth = document.body.clientWidth; // IE 4 compatible
			return intWidth;
		},

		// Used to set the focus to the passed element within the page. Typically used to set the initial focus.
		focusField: function (target) {
			if (target == null)
				target = document.getElementById(strElementIdToFocus);
			if (target == null)
				return;

			// The form elements that will be tested. Anything with a dot indicates the "type" attribute of the element.
			var formElements = ["input.text", "input.checkbox", "input.radio", "select", "textarea"];
			var selectedNode = null;

			// IE's selection method.
			if (typeof document.selection != "undefined" && document.selection != null && typeof window.opera == "undefined") {
				var theSelection = document.selection;
				var textRange = document.selection.createRange();
				selectedNode = textRange.parentElement();
			}
				// W3 selection method. Currently only Mozilla & Safari support it. However, neither of them support ranges inside form objects, so this part is redundant. Merely included in case they decide to include support in the future.
			else if (typeof window.getSelection != "undefined") {
				var theSelection = window.getSelection();

				// The Safari and then the Mozilla way to get the node that a selection starts in.
				if (typeof theSelection.baseNode != "undefined")
					selectedNode = theSelection.baseNode;
				else if (typeof theSelection.getRangeAt != "undefined" && theSelection.rangeCount > 0)
					selectedNode = theSelection.getRangeAt(0).startContainer;
			}

			// If a selected node was found above, check whether it's a selection inside one of the specified form element types.
			if (selectedNode != null)
				for (var i = 0; i < formElements.length; i++)
					if (selectedNode.nodeName.toLowerCase() == formElements[i].replace(/([^.]*)\..*/, "$1"))
						return;

			var forms = document.forms;

			// Do a check of each form element on the page. If one of them has a value, do not focus.
			for (i = 0; i < forms.length; i++) {
				var formElements = forms[i];
				for (var j = 0; j < formElements.length; j++) {
					if (formElements[j].getAttribute("type") == "checkbox" || formElements[j].getAttribute("type") == "radio") {
						if (formElements[j].checked != formElements[j].defaultChecked)
							return;
					}
					else {
						if (typeof formElements[j].defaultValue != "undefined" && formElements[j].value != formElements[j].defaultValue)
							return;
					}
				}
			}

			// If no form elements were found to be focused or with values, then go ahead and focus.
			target.focus();
			return;
		},

		// Adds an onkeydown event handler to the document. 
		handleFormOnKeyDown: function () {
			document.onkeydown = cw.Window.handleWindowKeystrokes;
			bolHandleFormKeystrokes = true;
		},

		// Handles all keystrokes within a window.
		handleWindowKeystrokes: function (oEvent) {
			var intKeyCode;
			var target;

			if (window.event) {
				intKeyCode = window.event.keyCode;
				target = window.event.target || window.event.srcElement;
			}
			else if (oEvent) {
				intKeyCode = oEvent.which;
				target = oEvent.target || oEvent.srcElement;
			}

			if (target.tagName != "TEXTAREA") {
				if (bolHandleFormKeystrokes) {
					bolHandleFormKeystrokes = false;
					if (intKeyCode == 13) {
						if (controlIdsToSkip.indexOf(target.id, 0) < 0) {
							if (window.event)
								window.event.keyCode = intKeyCode = 9;
							else
								return false;
						}
					}
				}
			}
		},

		//add control to skip enter test
		addControlIdToSkip: function (controlId) {
			if (controlIdsToSkip.indexOf(controlId, 0) < 0) {
				controlIdsToSkip += controlId + ",";
			}
		},

		// Scrolls to the specified position on the page.
		scrollTo: function (position, animate) {
			if ((typeof Ext != "undefined") && (position))
				Ext.get(document.documentElement || document.body).scrollTo('top', parseInt(position), animate);
		},

		// Looks inside a form element for a saved scroll offset. If found, then it scrolls to that position. Also injects an onsubmit function into the forms which stores the vertical offset.
		maintainScroll: function (formElement, memoryElement, animate) {
			formElement = document.forms[formElement];
			memoryElement = document.getElementById(memoryElement);

			if (memoryElement.value)
				cw.Window.scrollTo(memoryElement.value, animate);

			var storeVerticalOffset = function () { memoryElement.value = cw.Window.getVerticalOffset(); };

			if (typeof formElement.onsubmit != "function")
				formElement.onsubmit = storeVerticalOffset;
			else {
				var oldOnSubmit = formElement.onsubmit;
				formElement.onsubmit = function () { storeVerticalOffset(); return oldOnSubmit(); };
			}
		},

		// Returns the vertical offset of the top of the page.
		getVerticalOffset: function () {
			return typeof window.pageYOffset != 'undefined' ? window.pageYOffset : document.documentElement && document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop ? document.body.scrollTop : 0;
		},

		// Refreshes the event layers if the window is in a framed window and if the map is open.
		refreshEventLayers: function () {
			cw.Utilities.getRouter(function (r) {
				r.send('loadEventLayers', 'loadEventLayers');
			});
		},

		// Perform button click if button id is found
		performButtonClick: function (id) {
			var btn = document.getElementById(id);
			if (btn && typeof btn.onclick == "function") btn.onclick();
			else $('#' + id).trigger('click');
		},

		// Fires the passed mouse event exactly as if the mouse clicked on the element.
		fireMouseEvent: function (mouseEvent, element) {
			if (document.createEvent) {
				var oEvent = document.createEvent("MouseEvents");
				oEvent.initMouseEvent(mouseEvent, true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
				element.dispatchEvent(oEvent);
			}
		},
		getFrameSetState: function () {
			var pieces = new Array(intFrameToggleState, document.getElementById("FrameSet"));
			return pieces;
		}
	}; // End of cw.Window namespace.
}();

/***** cw.Pages.Search namespace *****/
cw.Pages.Search = function () {
	// Private members.
	var oGroupByGrid, strGroupByGridId, oGridContainer, oPagerDestination, strSearchResultsGridId, intPageCount, intTotalRecords, intHeightOffset = 150;

	// Used to set oGroupByGrid.
	function getGroupByGrid() {
		if ((igtbl_getGridById) && (strGroupByGridId))
			oGroupByGrid = igtbl_getGridById(strGroupByGridId);
	}

	return {
		// Helper method.
		setGroupByGridId: function (GroupByGridId) {
			strGroupByGridId = GroupByGridId;
		},

		// GroupByEntry object.
		GroupByEntry: function (intID, bolApplied, strText, strValue) {
			this.id = intID;
			this.applied = bolApplied;
			this.text = strText;
			this.value = strValue;

			this.getProperties = function () {
				var strProperties = "ID: " + this.id + "\n" +
                    "Applied: " + this.applied + "\n" +
                    "Text: " + this.text + "\n" +
                    "Value: " + this.value;
				return strProperties;
			};
		},

		// Used by a select box to update the entries in a GroupBy grid.
		updateGroupByGrid: function (oSelectBox) {
			if ((oSelectBox) && (oSelectBox.options)) {
				var arrGroupByEntries = new Array();
				var i, strRowId;
				for (i = 0; i < oSelectBox.options.length; i++) {
					if (oSelectBox.options[i].selected)
						arrGroupByEntries[i + ""] = new this.GroupByEntry(i, false, oSelectBox.options[i].text, oSelectBox.options[i].value);
				}

				if (oGroupByGrid == null)
					getGroupByGrid();

				if (oGroupByGrid) {
					// Remove any rows from the grid if they are not selected in the select box.
					var colGroupByRows = oGroupByGrid.Rows;
					for (i = 0; i < colGroupByRows.length; i++) {
						strRowId = colGroupByRows.getRow(i).getCell(0).getValue() + "";
						if (!arrGroupByEntries[strRowId]) {
							colGroupByRows.remove(i);
							i--;
						}
					}

					// Add any entries which are not already present to the grid.
					for (var strKey in arrGroupByEntries) {
						if (strKey != "length") {
							var oGroupByEntry = arrGroupByEntries[strKey];
							var bolFoundEntry = false;
							for (i = 0; i < colGroupByRows.length; i++) {
								strRowId = colGroupByRows.getRow(i).getCell(0).getValue() + "";
								if (strRowId == (oGroupByEntry.id + "")) {
									bolFoundEntry = true;
									i = colGroupByRows.length;
								}
							}

							if (!bolFoundEntry) {
								var oNewRow = colGroupByRows.addNew();
								oNewRow.getCell(0).setValue(oGroupByEntry.id);
								oNewRow.getCell(1).setValue(oGroupByEntry.applied);
								oNewRow.getCell(2).setValue(oGroupByEntry.text);
								oNewRow.getCell(3).setValue(oGroupByEntry.value);
							}
						}
					}
				}
			}
		},

		// Move the selected row in the GroupBy grid up one.
		moveUp: function () {
			if (oGroupByGrid == null)
				getGroupByGrid();

			cw.UltraGrid.moveSelectedRowUp(oGroupByGrid);
		},

		// Move the selected row in the GroupBy grid down one.
		moveDown: function () {
			if (oGroupByGrid == null)
				getGroupByGrid();
			cw.UltraGrid.moveSelectedRowDown(oGroupByGrid);
		},

		// Intended to set the grid height on the search results grid container element.
		setGridContainerHeight: function (gridContainerID) {
			if (oGridContainer == null)
				oGridContainer = document.getElementById(gridContainerID);

			if (oGridContainer) {
				var intNewGridHeight = cw.Window.getHeight() - intHeightOffset;
				oGridContainer.style.height = intNewGridHeight + "px";
			}
		},

		// Processes a new page that has been loaded into the search results grid.
		processNewPage: function (gridId) {
			try {
				// Get the new pager and replace the old one.
				var oPagerSource = document.getElementById("g-PagerSource");
				if (oPagerSource) {
					// Get the pager destination and cache it.
					if (oPagerDestination == null)
						oPagerDestination = document.getElementById("g-PagerDestination");

					if (oPagerDestination) {
						// Remove children from the pager copy's destination container.
						if (oPagerDestination.hasChildNodes())
							oPagerDestination.removeChild(oPagerDestination.firstChild);

						// Copy the pager and insert it into the new destination container. Also insert the total number of records.
						var oPagerCopy = oPagerSource.cloneNode(true);
						oPagerCopy.id = "g-PagerCopy";
						oPagerCopy.innerHTML = oPagerCopy.innerHTML.replace(/\{0\}/g, intTotalRecords);
						oPagerDestination.appendChild(oPagerCopy);

						// Prepare the go to textbox to handle the enter keystroke properly.
						var oGoToPageElement = oPagerCopy.getElementsByTagName("input")[0];
						if (oGoToPageElement) {
							oGoToPageElement.setAttribute("onkeydown", "return cw.Utilities.allowOnlyNumericKeys(event);");
							if (oGoToPageElement)
								Ext.get(oGoToPageElement).addKeyListener([10, 13], function () { cw.Pages.Search.goToPage(oGoToPageElement); });
						}
					}
				}
			}
			catch (e) { }
			// Add alternating row colors to this page.
			if (igtbl_getGridById) {
				var oGrid = igtbl_getGridById(gridId);
				if (oGrid) {
					var arrRows = oGrid.Rows;
					if (arrRows.length > 0 && typeof (this.expandAndStyleRows) != 'undefined')
						this.expandAndStyleRows(arrRows);
				}
			}
			cw.UltraGrid.autoSizeGrid(gridId);
		},

		// Called when initializing the search results grid. Sets things up for efficient operation.
		initializeResultsGrid: function (gridId) {
			$(function () {
				// Initialize and handle the pager.
				var oPagerSource = document.getElementById("g-PagerSource");
				if (oPagerSource) {
					oPagerSource.parentNode.parentNode.style.display = "none";
					cw.Pages.Search.processNewPage(gridId);
				}
				else {
					var oDestination = document.getElementById("g-PagerDestination");
					intHeightOffset = 130;
					oDestination.className = "noPager";
					oDestination.innerHTML = "Displaying " + intTotalRecords + " records.";
				}

				// Create zebra rows if this is the only page.
				if ((intPageCount == null) || (intPageCount <= 1))
					cw.Pages.Search.processNewPage(gridId);

				// Resizes the grid to fill the screen and set it to periodically resize the grid.
				if (cw.Print.printFriendly == false && intTotalRecords != 1 && typeof (resultsGridContainer) != 'undefined') {
					cw.Pages.Search.setGridContainerHeight(resultsGridContainer);
					window.onresize = function () { cw.Pages.Search.setGridContainerHeight(resultsGridContainer); };
				}

				// Save the grid id for later use by the goToPage method.
				strSearchResultsGridId = gridId;
			});
		},

		// Deep expands the passed row collection and makes it look decent.
		expandAndStyleRows: function (rows) {
			try {
				if (rows.getRow(0).ChildRowsCount > 0) {
					// If this row collection has groups, then expand those and make them look pretty.
					for (var i = 0, length = rows.length; i < length; i++) {
						var oRow = rows.getRow(i);
						oRow.setExpanded(true);
						this.expandAndStyleRows(oRow.getChildRows());
					}
				}
				else {
					// This collection doesn't have any groups. Make it look pretty.
					for (var i = 1, length = rows.length; i < length; i += 2)
						rows.getRow(i).Element.className = "g-Alternate";
				}
			}
			catch (e) { }
		},

		// Jumps the search results grid to the specified page.
		goToPage: function (element) {
			if (element.tagName.toLowerCase() == "button")
				element = element.parentNode.getElementsByTagName("input")[0];

			var intPage = getInputValue(element);
			if (intPage) {
				if (parseInt(intPage) > intPageCount)
					intPage = intPageCount;
				igtbl_getGridById(strSearchResultsGridId).goToPage(intPage);
			}
		},

		// Sets the total pages that are available to go through.
		setPageCount: function (pageCount) {
			intPageCount = pageCount;
		},

		// Gets the total pages that are available to go through.
		getPageCount: function () {
			return intPageCount;
		},

		// Sets the total record count for the current data set being displayed in the search results grid.
		setTotalRecordCount: function (totalRecords) {
			intTotalRecords = totalRecords;
		},

		// Gets the total record count for the current data set being displayed in the search results grid.
		getTotalRecordCount: function () {
			return intTotalRecords;
		}
	}; // End of cw.Pages.Search namespace.
}();

/***** cw.Print namespace *****/
cw.Print = function () {
	// Private members.
	var strPrintPreviewText = "This is the print preview page and has limited functionality.";
	var strPrintButtonText = "Print";
	var strBackButtonText = "Close Preview";

	return {
		printFriendly: false,
		printPage: function (strPrintContainer, boolUseHistory, funcPrintSetup) {
			// Mark this page as being printer friendly. This can be used to tweak some things through JavaScript for pages that are formatted to be printer friendly.
			cw.Print.printFriendly = true;

			//(JO) added boolUseHistory so we could switch when to use the history
			//     versus when to use the close window.  Close window is the default.
			if (strPrintContainer) {
				var oContainer = document.getElementById(strPrintContainer);
				var fnPrintSetup = funcPrintSetup;
				if (oContainer) {
					var oPrintPreview = document.createElement("span");
					oPrintPreview.className = "print";
					oPrintPreview.innerHTML = strPrintPreviewText;

					oPrintPreview.appendChild(document.createElement("br"));

					var oPrintButton = document.createElement("button");
					oPrintButton.className = "formButton";
					oPrintButton.onclick = function () { if (fnPrintSetup) fnPrintSetup(); window.print(); };
					oPrintButton.setAttribute("type", "button");
					oPrintButton.innerHTML = strPrintButtonText;
					oPrintPreview.appendChild(oPrintButton);

					var oBackButton = document.createElement("button");
					oBackButton.className = "formButton";
					if (boolUseHistory && boolUseHistory == true)
						oBackButton.onclick = function () { history.go(-1); };
					else
						oBackButton.onclick = function () { window.close(); };
					oBackButton.setAttribute("type", "button");
					oBackButton.innerHTML = strBackButtonText;
					oPrintPreview.appendChild(oBackButton);

					oContainer.appendChild(oPrintPreview);
				}
			}
			//window.print();
		}
	}; // End of cw.Print namespace.
}();

cw.UltraTree = function () {
	// Special utility function which parses a node and its children looking for something that is selected.
	function findSelectedNode(node) {
		// Make sure the node we're dealing with is valid.
		if (node == null)
			return null;

		// Check the current node.
		if (node.getSelected())
			return node;

		// Check sibling nodes.
		var oSelectedNode = findSelectedNode(node.getNextSibling());
		if (oSelectedNode != null)
			return oSelectedNode;

		// Check child nodes.
		if (node.hasChildren())
			return findSelectedNode(node.getFirstChild());
		return null;
	}

	return {
		// Gets the currently selected node within a tree.
		getSelectedNode: function (tree) {
			// Iterate through all of the nodes wothin the tree.
			var oNode;
			var arrNodes = tree.getNodes();
			for (var i = 0; i < arrNodes.length; i++) {
				// Return the current node if it is selected.
				if (arrNodes[i].getSelected()) {
					oNode = arrNodes[i];
					break;
				} else if (arrNodes[i].hasChildren()) {
					// Look within the current node's children for a selected node.
					oNode = findSelectedNode(arrNodes[i].getFirstChild());
					if (oNode)
						break;
				}
			}
			return oNode;
		},

		// Gets the first tree node that matches a specific tag.
		getNodeByTag: function (parentNode, tag) {
			if (parentNode) {
				// Check the current node.
				if (parentNode.getTag() == tag)
					return parentNode;

				// Check all of sibling nodes next to this node.
				var oTaggedNode = cw.UltraTree.getNodeByTag(parentNode.getNextSibling(), tag);
				if (oTaggedNode)
					return oTaggedNode;

				// Check this node's children.
				oTaggedNode = cw.UltraTree.getNodeByTag(parentNode.getFirstChild(), tag);
				if (oTaggedNode)
					return oTaggedNode;
			}
			return null;
		}
	};
}();

/***** cw.UltraGrid namespace *****/
cw.UltraGrid = function () {
	return {
		// Returns the first selected row.
		getFirstSelectedRow: function (oGroupByGrid) {
			if ((oGroupByGrid) && (igtbl_getLength(oGroupByGrid.Rows) > 0)) {
				var intRowCount = igtbl_getLength(oGroupByGrid.Rows);
				for (var i = 0; i < intRowCount; i++) {
					var oRow = oGroupByGrid.Rows.getRow(i);
					if ((oRow) && (oRow.getSelected()))
						return oRow;
				}
			}
			return null;
		},

		// Moves the currently selected row in the grid down by one. Untested on grids with nested rows.
		moveSelectedRowDown: function (oGroupByGrid) {
			if (oGroupByGrid) {
				var oRow = cw.UltraGrid.getFirstSelectedRow(oGroupByGrid);
				if (oRow) {
					var intIndex = oRow.getIndex() + 1;
					if (intIndex == oGroupByGrid.Rows.length - 1) {
						var oNewRow = oGroupByGrid.Rows.addNew();
						oRow.remove();
						oGroupByGrid.Rows.insert(oRow, intIndex);
						oNewRow.remove();
					}
					else
						if (intIndex < oGroupByGrid.Rows.length) {
							oRow.remove();
							oGroupByGrid.Rows.insert(oRow, intIndex);
						}
				}
			}
		},

		// Moves the currently selected row in the grid up by one. Untested on grids with nested rows.
		moveSelectedRowUp: function (oGroupByGrid) {
			if (oGroupByGrid) {
				var oRow = cw.UltraGrid.getFirstSelectedRow(oGroupByGrid);
				if (oRow) {
					var intIndex = oRow.getIndex() - 1;
					if (intIndex > -1) {
						oRow.remove();
						oGroupByGrid.Rows.insert(oRow, intIndex);
					}
				}
			}
		},

		// Gets the first row within a grid with a cell whose key and value match the passed values.
		getRowByKeyValue: function (grid, key, value) {
			// Only run if everything has been passed.
			if ((grid) && (key) && (value)) {
				// Initialize variables.
				var oRows = grid.Rows;
				var oRow;

				// Iterate through each row and check for a matching key/value combination.
				for (var i = 0; i < oRows.length; i++) {
					oRow = oRows.getRow(i);
					if (oRow.getCellFromKey(key).getValue() == value)
						return oRow;
				}
			}
			return null;
		},
		// Bind an infragistics grid to an object dataSource.  The dataSource is a object that has properties
		// that match the keys set for the columns, so column will be bound if a property is found that
		// has a matching key.
		dataBind: function (gridId, dataSource) {
			if (dataSource != null && dataSource.length > 0) {
				var props = []
				var grid = igtbl_getGridById(gridId);

				// Get a list of the properties on the data source that
				// map to columns in the grid.
				for (var prop in dataSource[0]) {
					if (grid.Bands[0].getColumnFromKey(prop) != null)
						props.push(prop);
				}

				// Add the rows to the grid.
				for (var i = 0; i < dataSource.length; i++) {
					row = grid.Rows.addNew();

					// Set the values of each cell based on the matching
					// of the grid's keys and the object property names.
					for (var j = 0; j < props.length; j++)
						row.getCellFromKey(props[j]).setValue(dataSource[i][props[j]]);
				}
			}
		},
		autoSizeGrid: function (gridId) {
			if (typeof (Ext) != 'undefined') {
				//IE is super slow running this routine.
				if (Ext.isIE) return;
			}
			if (typeof (igtbl_getGridById) == 'undefined') return;
			//from http://forums.infragistics.com/forums/t/4650.aspx
			try {
				// Retrieve the grid based on its ID.
				var oGrid = igtbl_getGridById(gridId);
				if (!oGrid) return;
				if (!oGrid.Bands) return;

				// Get the Bands collection
				var oBands = oGrid.Bands;
				if (!oBands) return;

				// Get the columns collection for Band 0 
				var oBand = oBands[0];
				if (!oBand) return;

				var oColumns = oBand.Columns;
				if (!oColumns) return;

				// The column count is equal to the
				// length of the Columns array.
				var count = oColumns.length;

				var colArray = null;
				// Initialise Column Size Array on the first call.
				if (colArray == null) {
					colArray = new Array();
					for (var i = 0; i < count; i++) {
						colArray[i] = 0;
					}
				}

				// Set each Column Header width.
				var oColHeaders;
				for (var i = 0; i < count; i++) {
					// Don't check columns that aren't displayed
					if (!oColumns[i].ServerOnly && !oColumns[i].Hidden) {
						// This loop is needed since each Column Header 
						// have the same Id in every group. We need to
						// check them all because only opened groups will
						// have a value in "offsetWidth".
						oColHeaders = oGrid.MainGrid.getElementsByTagName('th')
						for (var j = 0; j < oColHeaders.length; j++) {
							if (oColHeaders[j].getAttribute('id') == oColumns[i].Id) {
								var colWidth = oColHeaders[j].firstChild.offsetWidth
								if (colArray[i] < colWidth) {
									colArray[i] = colWidth;
								}
								if (colWidth > 0) {
									// We found a displayed header, no need to check them all.
									break;
								}
							}
						}
					}
				}

				// Get the width from the data rows
				this.ParseNonGroupingRows(oGrid.Rows, oColumns, colArray);

				// Loop through each Column in the grid to set
				// the Column width to the largest width found.
				for (var i = 0; i < count; i++) {
					// Don't set the width for columns that weren't available
					if (colArray[i] > 0) {
						// Note: the 10 pixels padding here might need to be 
						// adjusted depending on the layout and styles of the grid.
						oColumns[i].setWidth(colArray[i] + 10);
					}
				}
			}
			catch (e) {
			}
		},
		ParseNonGroupingRows: function (rowList, colList, colWidthArray) {
			// This function goes through each level of grouping rows 
			// in order to get to the data rows
			if (rowList.getRow(0).GroupByRow) {
				// Check if the first row is a GroupBy row
				for (var i = 0; i < rowList.length; i++) {
					// If so, call the function recursively for each 
					// GroupBy row in the list, passing out it's 
					// (child) Rows collection to the function.
					this.ParseNonGroupingRows(rowList.getRow(i).Rows, colList, colWidthArray)
				}
			} else {
				// The first row is a data row, parse all the rows
				// in the collection to get their cells width.
				this.GetRowsCellWidth(rowList, colList, colWidthArray);
			}
		},
		GetRowsCellWidth: function (rowList, colList, colWidthArray) {
			// This function do the actual job of getting the columns 
			// width from each cell

			for (var i = 0; i < rowList.length; i++) {
				for (var n = 0; n < colList.length; n++) {
					// Don't check columns that aren't displayed
					if (!colList[n].ServerOnly && !colList[n].Hidden) {
						var cell = rowList.getRow(i).getCell(n);
						if (colWidthArray[n] < cell.Element.firstChild.offsetWidth) {
							colWidthArray[n] = cell.Element.firstChild.offsetWidth;
						}
					}
				}
			}

		},
		autoSizeGridOLD: function (gridId) {
			try {
				var grid = igtbl_getGridById(gridId);
				var band = grid.Bands[0];
				var columns = band.Columns;
				var rows = grid.Rows;

				for (var columnIndex = 0; columnIndex < columns.length; columnIndex++) {
					var col = columns[columnIndex];
					var length = 0;
					for (var rowIndex = 0; rowIndex < rows.length; rowIndex++) {
						var row = rows.getRow(rowIndex);
						var cell = row.getCell(columnIndex);

						if (cell.Element.firstChild.offsetWidth > length) {
							length = cell.Element.firstChild.offsetWidth;
						}
					}

					col.setWidth(length);
				}
			}
			catch (e) {
			}
		}
	}; // End of cw.UltraGrid namespace.
}();

/***** cw.Shortcut namespace *****/
cw.Shortcut = {
	'all_shortcuts': {}, //All the shortcuts are stored in this array
	'add': function (shortcut_combination, callback, opt) {
		//Provide a set of default options
		var default_options = {
			'type': 'keydown',
			'propagate': false,
			'disable_in_input': false,
			'target': document,
			'keycode': false
		}
		if (!opt)
			opt = default_options;
		else {
			for (var dfo in default_options) {
				if (typeof opt[dfo] == 'undefined')
					opt[dfo] = default_options[dfo];
			}
		}

		var ele = opt.target
		if (typeof opt.target == 'string')
			ele = document.getElementById(opt.target);
		var ths = this;
		shortcut_combination = shortcut_combination.toLowerCase();

		//The function to be called at keypress
		var func = function (e) {
			e = e || window.event;

			if (opt['disable_in_input']) { //Don't enable shortcut keys in Input, Textarea fields
				var element;
				if (e.target)
					element = e.target;
				else if (e.srcElement)
					element = e.srcElement;
				if (element.nodeType == 3)
					element = element.parentNode;

				if (element.tagName == 'INPUT' || element.tagName == 'TEXTAREA')
					return;
			}

			//Find Which key is pressed
			if (e.keyCode)
				code = e.keyCode;
			else if (e.which)
				code = e.which;
			var character = String.fromCharCode(code).toLowerCase();

			if (code == 188)
				character = ","; //If the user presses , when the type is onkeydown
			if (code == 190)
				character = "."; //If the user presses , when the type is onkeydown

			var keys = shortcut_combination.split("+");
			//Key Pressed - counts the number of valid keypresses - if it is same as the number of keys, the shortcut function is invoked
			var kp = 0;

			//Work around for stupid Shift key bug created by using lowercase - as a result the shift+num combination was broken
			var shift_nums = {
				"`": "~",
				"1": "!",
				"2": "@",
				"3": "#",
				"4": "$",
				"5": "%",
				"6": "^",
				"7": "&",
				"8": "*",
				"9": "(",
				"0": ")",
				"-": "_",
				"=": "+",
				";": ":",
				"'": "\"",
				",": "<",
				".": ">",
				"/": "?",
				"\\": "|"
			};
			//Special Keys - and their codes
			var special_keys = {
				'esc': 27,
				'escape': 27,
				'tab': 9,
				'space': 32,
				'return': 13,
				'enter': 13,
				'backspace': 8,

				'scrolllock': 145,
				'scroll_lock': 145,
				'scroll': 145,
				'capslock': 20,
				'caps_lock': 20,
				'caps': 20,
				'numlock': 144,
				'num_lock': 144,
				'num': 144,

				'pause': 19,
				'break': 19,

				'insert': 45,
				'home': 36,
				'delete': 46,
				'end': 35,

				'pageup': 33,
				'page_up': 33,
				'pu': 33,

				'pagedown': 34,
				'page_down': 34,
				'pd': 34,

				'left': 37,
				'up': 38,
				'right': 39,
				'down': 40,

				'f1': 112,
				'f2': 113,
				'f3': 114,
				'f4': 115,
				'f5': 116,
				'f6': 117,
				'f7': 118,
				'f8': 119,
				'f9': 120,
				'f10': 121,
				'f11': 122,
				'f12': 123
			};

			var modifiers = {
				shift: { wanted: false, pressed: false },
				ctrl: { wanted: false, pressed: false },
				alt: { wanted: false, pressed: false },
				meta: { wanted: false, pressed: false }    //Meta is Mac specific
			};

			if (e.ctrlKey)
				modifiers.ctrl.pressed = true;
			if (e.shiftKey)
				modifiers.shift.pressed = true;
			if (e.altKey)
				modifiers.alt.pressed = true;
			if (e.metaKey)
				modifiers.meta.pressed = true;

			for (var i = 0; k = keys[i], i < keys.length; i++) {
				//Modifiers
				if (k == 'ctrl' || k == 'control') {
					kp++;
					modifiers.ctrl.wanted = true;

				} else if (k == 'shift') {
					kp++;
					modifiers.shift.wanted = true;

				} else if (k == 'alt') {
					kp++;
					modifiers.alt.wanted = true;
				} else if (k == 'meta') {
					kp++;
					modifiers.meta.wanted = true;
				} else if (k.length > 1) { //If it is a special key
					if (special_keys[k] == code)
						kp++;

				} else if (opt['keycode']) {
					if (opt['keycode'] == code)
						kp++;

				} else { //The special keys did not match
					if (character == k)
						kp++;
					else {
						if (shift_nums[character] && e.shiftKey) { //Stupid Shift key bug created by using lowercase
							character = shift_nums[character];
							if (character == k)
								kp++;
						}
					}
				}
			}

			if (kp == keys.length &&
                modifiers.ctrl.pressed == modifiers.ctrl.wanted &&
                modifiers.shift.pressed == modifiers.shift.wanted &&
                modifiers.alt.pressed == modifiers.alt.wanted &&
                modifiers.meta.pressed == modifiers.meta.wanted) {
				callback(e);

				if (!opt['propagate']) { //Stop the event
					//e.cancelBubble is supported by IE - this will kill the bubbling process.
					e.cancelBubble = true;
					e.returnValue = false;

					//e.stopPropagation works in Firefox.
					if (e.stopPropagation) {
						e.stopPropagation();
						e.preventDefault();
					}
					return false;
				}
			}
		};
		this.all_shortcuts[shortcut_combination] = {
			'callback': func,
			'target': ele,
			'event': opt['type']
		};
		//Attach the function with the event
		if (ele.addEventListener)
			ele.addEventListener(opt['type'], func, false);
		else if (ele.attachEvent)
			ele.attachEvent('on' + opt['type'], func);
		else
			ele['on' + opt['type']] = func;
	},

	//Remove the shortcut - just specify the shortcut and I will remove the binding
	'remove': function (shortcut_combination) {
		shortcut_combination = shortcut_combination.toLowerCase();
		var binding = this.all_shortcuts[shortcut_combination];
		delete (this.all_shortcuts[shortcut_combination])
		if (!binding)
			return;
		var type = binding['event'];
		var ele = binding['target'];
		var callback = binding['callback'];

		if (ele.detachEvent)
			ele.detachEvent('on' + type, callback);
		else if (ele.removeEventListener)
			ele.removeEventListener(type, callback, false);
		else
			ele['on' + type] = false;
	}
};

/***** cw.Utilities namespace *****/
cw.Utilities = function () {
	var _verificationNeededForConfirm = true;
	var _confirmButtonId = '';
	var _reopenId = '';

	var _bypassYesNoMessage = false;
	function handleYesNoConfirmationResponse(results, buttonId, ctrlId) {
		var oCtrl = document.getElementById(ctrlId);
		var resp = (results == "yes").toString();
		if (oCtrl) setInputValue(oCtrl, resp);
		_bypassYesNoMessage = true;
		cw.Window.performButtonClick(buttonId);
	}

	return {
		//show or hide an element
		showHideElement: function (ids, displayValue) {
			var tmpIds = '';
			if (typeof (ids) == 'object')
				tmpIds = ids;
			if (typeof (ids) == 'string')
				tmpIds = new Array(ids);
			if (tmpIds != '') {
				for (var i in tmpIds) {
					var oId = tmpIds[i];
					if (typeof (oId) != 'undefined') {
						var oElement = document.getElementById(oId);
						if ((oElement != null) && (oElement.style))
							oElement.style.display = displayValue;
					}
				}
			}
			return false;
		},
		//make sure start date is before finish date
		validateFinishDate: function (dateStartInputId, dateFinishInputId) {
			//default to true
			var oValidDate = true;
			var oStart = igedit_getById(dateStartInputId);
			var oFinish = igedit_getById(dateFinishInputId);

			if (oStart && oFinish) {
				try {
					//get date values
					var startTime = oStart.getValue();
					var finishTime = oFinish.getValue();

					if (isNaN(startTime) || startTime == null)
						startTime = -9999;
					if (isNaN(finishTime) || finishTime == null)
						finishTime = -9999;

					//only check when there is actually a value in finishTime
					if (finishTime >= 0) {
						var diff = finishTime - startTime;

						//valid if diff is positive (means finish is after start)
						oValidDate = diff >= 0;
					}
				} catch (err) {
				}
			}
			return oValidDate;
		},
		trimLength: function (obj) {
			try {
				var mlength = obj.getAttribute ? parseInt(obj.getAttribute("maxlength")) : ""
				if (obj.getAttribute && obj.value.length > mlength)
					obj.value = obj.value.substring(0, mlength)
			} catch (err) {
			}
		},
		// Used in conjunction with keystroke events. Cancels the event if the keystroke is not a number.
		allowOnlyNumericKeys: function (e) {
			// Get the numeric key code that was just entered.
			var intKeyCode;
			if (window.event)
				intKeyCode = e.keyCode;
			else if (e.which)
				intKeyCode = e.which;

			// Determine if it was numeric. If it isn't, then cancel the event.
			if (intKeyCode)
				return (intKeyCode == 8 || intKeyCode == 9 || intKeyCode == 10 || intKeyCode == 13 || intKeyCode == 45 || intKeyCode == 46 || (intKeyCode >= 35 && intKeyCode <= 40) || (intKeyCode >= 48 && intKeyCode <= 57) || (intKeyCode >= 96 && intKeyCode <= 107))
		},
		// A very simple function used to time the execution time of a function.
		// An console message will be logged with the time in milliseconds.
		//
		// Examples:

		// This
		//  var myvalue = myfunc1(data);
		//  myfunc2(myvalue);
		// Becomes
		//  var myvalue = cw.Utilities.timeIt(function() { return myfunc1(data); });
		//  cw.Utilities.timeIt(function() { myfunc2(myvalue); });
		//
		// NOTE: Note the return on the first example when a return value is needed.
		//
		timeIt: function (func) {
			var start = (new Date()).getTime();
			var ret = func();
			var end = (new Date()).getTime();
			if (typeof console !== "undefined")
				console.log(end - start);
			else
				alert(end - start);
			return ret;
		},
		// Parse the json string and return the created object.
		//
		// NOTE: This function implements a fixup so that dates are properly created.
		// The +1 to the date was added because for some reason the time was a second off.
		//
		parseJson: function (data) {
			var ret = JSON.parse(data, function (key, value) {
				if (typeof value === 'string') {
					var isDate = /Date\(([-+]?\d+[-+]?\d+)\)/.exec(value);
					if (isDate) {
						value = new Date(eval(isDate[1]));
						value.setSeconds(value.getSeconds() + 1);
					}
				}
				return value;
			});
			return ret;
		},
		doConfirm: function (buttonResults) {
			var tmpId = _confirmButtonId;
			_confirmButtonId = '';
			if (buttonResults == "yes") {
				_verificationNeededForConfirm = false;
				cw.Window.performButtonClick(tmpId);
			}
		},
		confirmCwItem: function (confirmButtonId, confirmTitle, confirmMessage) {
			var res = true;
			if (_verificationNeededForConfirm == true) {
				_confirmButtonId = confirmButtonId;

				if (typeof (Ext) != 'undefined') {
					Ext.Msg.show({
						title: confirmTitle,
						msg: confirmMessage,
						buttons: Ext.Msg.YESNO,
						fn: this.doConfirm,
						icon: Ext.MessageBox.WARNING
					});
				}
				res = false;
			}
			_verificationNeededForConfirm = true;
			return res;
		},
		getGridRowCount: function (gridId) {
			var grid = igtbl_getGridById(gridId);
			return grid.Rows.length;
		},
		getGridSelectedRowCount: function (gridId) {
			var ret = 0;
			var grid = igtbl_getGridById(gridId);

			// count all the selected rows.
			for (var i = 0; i < grid.Rows.length; i++) {
				var row = grid.Rows.getRow(i);
				if (row.getSelected())
					ret++;
			}

			return ret;
		},
		selectGridRow: function (gridId, text) {
			var grid = igtbl_getGridById(gridId);

			// find the row that contains the text and select it.
			for (var i = 0; i < grid.Rows.length; i++) {
				var row = grid.Rows.getRow(i);
				if (row.find(text) != null) {
					row.setSelected();
				}
			}
		},
		getAjaxCallUrl: function (postfix, ajaxCall, params) {
			var ret = window.location.href;

			// apply the postfix
			if (postfix) {
				var index = ret.search(/.aspx/i);
				ret = ret.substring(0, index) + postfix + ret.substring(index);
			}

			// handle the addition of the querystring parameter
			if (ret.substring(ret.length - 5, ret.length) == '.aspx')
				ret += '?';
			else
				ret += '&';

			ret += 'ajaxcall=' + ajaxCall;

			// apply the parameter if necessary
			if (params) {
				for (var i = 0; i < params.length; i++)
					ret += '&' + params[i].name + '=' + encodeURIComponent(params[i].value);
			}

			return ret;
		},
		getFirstLayoutManager: function () {
			var _layout = null;
			try {
				for (var i in cw.LayoutManagers) {
					if (typeof (cw.LayoutManagers[i].Messages) != 'undefined') {
						_layout = cw.LayoutManagers[i];
						break;
					}
				}
			} catch (e) {
			}
			return _layout;
		},
		boolReopening: false,
		doReOpenItem: function (buttonResults) {
			var tmpId = _confirmButtonId;
			_confirmButtonId = '';
			if (buttonResults == "yes") {
				var oReopen = document.getElementById(_reopenId);
				if (oReopen) {
					setInputValue(oReopen, 'y');
					_verificationNeededForConfirm = false;
					cw.Utilities.boolReopening = true;
					cw.Window.performButtonClick(tmpId);
					cw.Utilities.boolReopening = false;
				}
			}
		},
		confirmReOpenItem: function (confirmButtonId, confirmTitle, confirmMessage) {
			var res = true;
			if (_verificationNeededForConfirm == true) {
				_confirmButtonId = confirmButtonId;

				if (typeof (Ext) != 'undefined') {
					Ext.Msg.show({
						title: confirmTitle,
						msg: confirmMessage,
						buttons: Ext.Msg.YESNO,
						fn: this.doReOpenItem,
						icon: Ext.MessageBox.WARNING
					});
				}
				res = false;
			}
			_verificationNeededForConfirm = true;
			return res;
		},
		reopenRecord: function (cwId, reopenId, saveId) {
			_reopenId = reopenId;
			var oId = document.getElementById(cwId);
			var oIdValue = '';
			if (oId)
				oIdValue = getInputValue(oId, ',');
			if (oIdValue.toString().indexOf(',') > 0)
				oIdValue = oIdValue.toString().substr(0, oIdValue.toString().indexOf(','));

			var oLayout = this.getFirstLayoutManager();
			var title = oLayout.Messages.ReOpenTitle + ' ' + oIdValue;
			var message = oLayout.Messages.ReOpenText + ' ' + oIdValue + '?';
			return cw.Utilities.confirmReOpenItem(saveId, title, message);
		},
		setFromStreetCode: function (pieces, cityId, stateId, zipId) {
			var oTxt = document.getElementById(cityId);
			if (oTxt && pieces[1])
				setInputValue(oTxt, pieces[1]);

			oTxt = document.getElementById(stateId);
			if (oTxt && pieces[2])
				setInputValue(oTxt, pieces[2]);

			oTxt = document.getElementById(zipId);
			if (oTxt && pieces[3])
				setInputValue(oTxt, pieces[3]);
		},
		parseStreetCodes: function (itm, args, delimiter, cityId, stateId, zipId) {
			if (itm) {
				var textPieces = itm._element.value.toString().split(delimiter);
				var newText = textPieces[0];
				itm._element.value = newText;
				this.setFromStreetCode(textPieces, cityId, stateId, zipId);
			}
		},
		yesNoConfirm: function (confirmButtonId, responseControlId, title, text) {
			if (_bypassYesNoMessage) {
				_bypassYesNoMessage = false;
				return true;
			}

			Ext.Msg.show({
				title: title,
				msg: text,
				buttons: Ext.Msg.YESNO,
				fn: function (buttonResults) { handleYesNoConfirmationResponse(buttonResults, confirmButtonId, responseControlId); },
				icon: Ext.MessageBox.WARNING
			});
			return false;
		},
		inlineConfirm: function (confirmButtonId, text, yesText, noText, callback) {
			var btn = $get(confirmButtonId);
			if (!btn)
				return false;
			if (!btn.confirm) {
				btn.showConfirmation = true;
				var s = document.createElement('span');
				var y = document.createElement('input'), n = document.createElement('input');
				n.type = y.type = 'button';
				n.className = 'formButton';
				y.className = 'formButton';
				y.value = yesText;
				n.value = noText;
				n.id = btn.id + 'Cancel';
				btn.parentNode.insertBefore(s, btn.parentNode.firstChild);
				btn.confirm = new Ext.Container({ autoEl: 'span', style: 'position:absolute;min-height:20px;z-index:1;background-color:' + Ext.get(btn.parentNode).getColor('background-color', 'white'), items: [{ xtype: 'label', text: text + ' ' }, new Ext.Component(y), new Ext.Component(n)], applyTo: s });
				Ext.EventManager.on(y.id, 'click', function () {
					btn.showConfirmation = false;
					callback('yes');
					btn.showConfirmation = true;
				});
				Ext.EventManager.on(n.id, 'click', function () {
					btn.confirm.hide();
					callback('cancel');
				});
			} else if (!btn.showConfirmation)
				return true;
			else
				btn.confirm.show();
			return false;
		},
		getRouter: function (callBack) {
			if (cw.router) {
				callBack(cw.router);
			} else {
				require(['js/factory'], function (factory) {
					if (factory) {
						var r = factory.createRouter("cw-router");
						if (cw && cw.router) callBack(cw.router); else callBack(r);
					}
				});
			}
		}
	};
}();

/***** event hub used for ui iteraction *****/
cw.EventHub = function () {
	var messages = [];

	return {
		subscribe: function (message, key, func) {
			// setup the message subscriber list of this is a new message
			if (!messages[message])
				messages[message] = [];

			// add the subscriber function
			messages[message][key] = func;
		},
		unsubscribe: function (message, key) {
			delete messages[message][key];
		},
		send: function (message, data) {
			if (message) {
				if (messages[message]) {
					for (var key in messages[message])
						messages[message][key](data);
				}
				else if (typeof console !== "undefined")
					console.log('EVENTHUB: Message type (' + message + ') does not exist.');
			}
			else if (typeof console !== "undefined")
				console.log('EVENTHUB: Undefined message type.');
		}
	};
}();

cw.Dictionary = function (test) {
	function isKey(k) {
		var ret = true;
		if (k == 'set' || k == 'get' || k == 'exists' || k == 'remove' || k == 'keys' || k == 'values' || k == 'kvps')
			ret = false;
		return ret;
	}

	var iStore = {};

	return {
		set: function (key, value) {
			if (isKey(key))
				this[key] = function () { return value; }();
			else
				iStore[key] = function () { return value; }();
		},
		get: function (key) {
			if (isKey(key))
				return this[key];
			return iStore[key];
		},
		exists: function (key) {
			if (isKey(key))
				return this[key] ? true : false;
			return iStore[key] ? true : false;
		},
		remove: function (key) {
			if (isKey(key))
				delete this[key];
			else
				delete iStore[key];
		},
		keys: function () {
			var ret = [];
			for (var i in this) {
				if (isKey(i))
					ret[ret.length] = i;
			}
			for (var j in iStore)
				ret[ret.length] = j;
			return ret;
		},
		values: function () {
			var ret = [];
			for (var i in this) {
				if (isKey(i))
					ret[ret.length] = this[i];
			}
			for (var j in iStore) {
				ret[ret.length] = iStore[j];
			}
			return ret;
		},
		kvps: function () {
			var ret = {};
			for (var i in this) {
				if (isKey(i))
					ret[i] = this[i];
			}
			for (var j in iStore)
				ret[j] = iStore[j];
			return ret;
		}
	};
};

// this is used to construct the object for representing layout managers and their definitions
cw.LayoutManagers = new cw.Dictionary();

cw.onReady = addLoadEvent;

/***** animation used for fade in/out of element *****/
cw.animate = function () {
	var _isAnimating = false;
	return {
		set_isAnimating: function (val) {
			_isAnimating = val;
		},
		isAnimating: function () {
			return _isAnimating;
		},
		opacity: function (id, opacStart, opacEnd, millisec) {
			//speed for each frame
			var speed = Math.round(millisec / 100);
			var timer = 0;

			//determine the direction for the blending, if start and end are the same nothing happens
			if (opacStart > opacEnd) {
				for (i = opacStart; i >= opacEnd; i--) {
					setTimeout("cw.animate.changeOpac(" + i + ",'" + id + "')", (timer * speed));
					timer++;
				}
			} else if (opacStart < opacEnd) {
				for (i = opacStart; i <= opacEnd; i++) {
					setTimeout("cw.animate.changeOpac(" + i + ",'" + id + "')", (timer * speed));
					timer++;
				}
			}
		},
		changeOpac: function (opacity, id) {
			//change the opacity for different browsers
			try {
				var object = document.getElementById(id).style;
				object.opacity = (opacity / 100);
				object.MozOpacity = (opacity / 100);
				object.KhtmlOpacity = (opacity / 100);
				object.filter = "alpha(opacity=" + opacity + ")";

				_isAnimating = (opacity > 0 || opacity < 100);
			}
			catch (e) {
			}
		},
		shiftOpacity: function (id, millisec) {
			//if an element is invisible, make it visible, else make it invisible
			if (document.getElementById(id).style.opacity == 0) {
				this.opacity(id, 0, 100, millisec);
			} else {
				this.opacity(id, 100, 0, millisec);
			}
		},
		blendimage: function (divid, imageid, imagefile, millisec) {
			var speed = Math.round(millisec / 100);
			var timer = 0;

			//set the current image as background
			document.getElementById(divid).style.backgroundImage = "url(" + document.getElementById(imageid).src + ")";

			//make image transparent
			this.changeOpac(0, imageid);

			//make new image
			document.getElementById(imageid).src = imagefile;

			//fade in image
			for (i = 0; i <= 100; i++) {
				setTimeout("cw.animate.changeOpac(" + i + ",'" + imageid + "')", (timer * speed));
				timer++;
			}
		},
		currentOpac: function (id, opacEnd, millisec) {
			//standard opacity is 100
			var currentOpac = 100;

			//if the element has an opacity set, get it
			if (document.getElementById(id).style.opacity < 100) {
				currentOpac = document.getElementById(id).style.opacity * 100;
			}

			//call for the function that changes the opacity
			this.opacity(id, currentOpac, opacEnd, millisec)
		}
	};
}();

cw.includeScriptFile = function (pagePath, scriptPath) {
	if (window.location.href.toLowerCase().indexOf(pagePath.toLowerCase()) > -1) {
		var scriptElement = document.createElement("script");
		scriptElement.src = scriptPath;
		document.documentElement.appendChild(scriptElement);
	}
};

// work around for Ext, IE9 not supporting Range.createContectualFragment
if (typeof Range !== 'undefined' && typeof Range.prototype.createContextualFragment === 'undefined') {
	Range.prototype.createContextualFragment = function (a) {
		var b = window.document;
		var c = b.createElement('div');
		c.innerHTML = a;
		var d = b.createDocumentFragment(), e;
		if ((e = c.firstChild)) {
			d.appendChild(e);
		}
		return d;
	};
}